import { Component } from "react";
import cat from "../assets/images/cat.png"
import InputMessage from "./inputMessage/InputMessage";
import OutputMessage from "./outputMesssage/OutputMessage";
class Animal extends Component {
    constructor(props) {
        super(props)
            this.state = {
                inputMessage: "",
                outputMessage: false,
                imgCat: false
            }
    }
    inputMessageChangeHandler = (value) => {
        this.setState({
            inputMessage: value
        })
        if(value === "cat") {
            this.setState({
                imgCat: true,
                outputMessage: false
            })
        }
        if(value !== "cat") {
            this.setState({
                imgCat: false,
                outputMessage: true
            })
        }
        if(value === ""){
            this.setState({
                outputMessage: false
            })
        }
    }
    render() {
        return(
            <>
            <InputMessage inputMessageProps = {this.state.inputMessage} inputMessageChangeHandlerProps = {this.inputMessageChangeHandler} outputMessageChangeHandlerProps={this.outputMessageChangeHandler}/>
            <OutputMessage catImgProps = {this.state.imgCat} outputMessageProps={this.state.outputMessage}/>
            </>
        )
    }
}

export default Animal;