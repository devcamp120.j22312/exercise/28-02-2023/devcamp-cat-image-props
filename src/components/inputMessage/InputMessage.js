import { Component } from "react";

class InputMessage extends Component {
    onInputChangeHandler = (event) => {
        const { inputMessageChangeHandlerProps } = this.props;
        inputMessageChangeHandlerProps(event.target.value);
    }
    render() {
        const { inputMessageProps } = this.props;
        return(
            <>
                <input style={{display:"flex", margin:"auto", marginTop:"20px" }} onChange={this.onInputChangeHandler} value={inputMessageProps}/>
            </>
        )
    }
}

export default InputMessage;