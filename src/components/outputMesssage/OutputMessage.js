import { Component } from "react";

class OutputMessage extends Component {
    render() {
        const {outputMessageProps, catImgProps} = this.props;
        return(
            <>
            {outputMessageProps ? <p style={{textAlign:"center", color:"blue"}}>meow not found</p> : null}
            {catImgProps ? <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/541117/cat.jpg" width="500px" /> : null}
            </>
        )
    }
}

export default OutputMessage;